from flask_restx import reqparse


def pi_parser():
    pi = reqparse.RequestParser()

    pi.add_argument(
        "n",
        type=int,
        dest="n",
        location="args",
        help="value to get n_th for Fibonacci sequence"
    )

    return pi
   