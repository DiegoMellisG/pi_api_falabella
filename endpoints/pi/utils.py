import numpy as np

def fibonacci(n):
    if n<0:
        return 
    elif n>12:
        return 
    # First Fibonacci number is 0
    elif n == 0:
        return 0
    # Second Fibonacci number is 1
    elif n == 1:
        return 1
    else:
        return fibonacci(n-1)+fibonacci(n-2)

def get_n_decimals_pi(n):
    if n < 0:
        return "%d"%(np.pi)
    elif n > 12:
        return "%f"%(np.pi)
    else:
        return "%.{}f".format(fibonacci(n))%(np.pi)

def get_n_digit(value, n):
    value = str(value)
    # If value not contain '.' is because only have the integer part
    if value.find('.') != -1:
        _ , decimals = value.split('.')
        if len(decimals) >= n:
            return decimals[n-1]
        else:
            return -1 
    else:
        return -1