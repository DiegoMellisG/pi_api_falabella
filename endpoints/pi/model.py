from flask_restx import fields, Model

pi_item = Model(
    "Pi",
    {
        "pi-fibonacci": fields.String(description="Pi number with n decimals"),
        "n-decimal": fields.String(description="Nth decimal from pi number")
    }
)