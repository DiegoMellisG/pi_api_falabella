from flask_restx import Namespace, Resource
from flask import request
from endpoints.pi.parser import pi_parser
from endpoints.pi.model import pi_item
from .utils import get_n_digit, get_n_decimals_pi


endpoint = Namespace("pi", description="Pi related operations")
endpoint.models[pi_item.name] = pi_item
parser = pi_parser()

@endpoint.route('/')
@endpoint.param('n', "Number to get n value of fibonacci sequence", type=int)
@endpoint.response(200, "Success")
@endpoint.response(404, "Not found")
class Pi(Resource):
    @endpoint.expect(pi_parser)
    @endpoint.marshal_with(pi_item)
    def get(self):
        args = parser.parse_args()
        print(args)
        pi_fibonacci = get_n_decimals_pi(args['n'])
        n_decimal = get_n_digit(pi_fibonacci, args['n'])
        
        return {'pi-fibonacci': pi_fibonacci,
        'n-decimal': n_decimal}
