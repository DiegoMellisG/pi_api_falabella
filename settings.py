PROFILE = False

PROFILE_CONFIG = {
    "enabled": PROFILE,
    "storage": {"engine": "sqlite"},
    "basicAuth": {"enabled": True, "username": "admin", "password": "admin"},
    "ignore": ["^/static/.*"],
}