from flask import Flask
from flask_restx import Api
from endpoints.pi.endpoint import endpoint as pi_endpoint

def create_app(config):
    app = Flask(__name__)

    # Setup the Flask-JWT-Extended extension
    app.config["JWT_SECRET_KEY"] = "super-secret"  # Change this!
    

    api = Api(app,
              title="Pi API",
              version="1.0.0")

    api.add_namespace(pi_endpoint, path="/pi")

    return app

app_service = create_app("settings")

if __name__ == '__main__':
    app_service.run(host="0.0.0.0",debug=True)


