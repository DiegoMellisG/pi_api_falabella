# PI API

This is an API to do some calculations according to the requirements from Falabella AI Area. This is a practical exercise to to apply for the Junior Developer position.

# Requirements

- Docker
- docker-compose
- Python 3.6 or greater

# Endpoints

| HTTP REQUEST  | Endpoint            | Description       |
|--------|-------------------|------------------------------------------------------------------------|
| [GET]  | /pi/?n={}            | Is the endpoint to get the response with the pi calculations using Fibonacci sequence|

# Response

The response has the following format if we use **/pi/?n=5** endpoint.

```json
{
	"pi-fibonacci": "3.14159",
	"n-decimal": "9"
}
```

# How to deploy 

In this case, Docker and docker-compose were used for the deployment, so it is necessary to have both tools installed and then you can run the following command from the the folder where `docker-compose.yml` is located to deploy this API.

```
docker-compose up --build -d
```
The command that is above will show this output in the terminal

```bash
Building flask-pi
Sending build context to Docker daemon  111.6kB
Step 1/8 : FROM python:3.6
 ---> 0c8ae2a24dca
Step 2/8 : ADD requirements.txt /app/
 ---> Using cache
 ---> b2e4cf935c6c
Step 3/8 : WORKDIR /app
 ---> Using cache
 ---> c328a40c825b
Step 4/8 : RUN pip install --upgrade pip
 ---> Using cache
 ---> 563e585f4df3
Step 5/8 : RUN pip install -r requirements.txt
 ---> Using cache
 ---> a00c273ab1a9
Step 6/8 : COPY . /app
 ---> 256c3589bf70
Step 7/8 : EXPOSE 5000
 ---> Running in ebde153ba2c4
Removing intermediate container ebde153ba2c4
 ---> de3120580205
Step 8/8 : CMD [ "python3", "app.py"]
 ---> Running in 08b4c397384e
Removing intermediate container 08b4c397384e
 ---> 6cc011351b59
Successfully built 6cc011351b59
Successfully tagged pi-api:latest
Recreating pi_api_flask-pi_1 ... done

```

Now, you need to get the logs from the container to get the host where the API is located. To do that you need to run the following command.

```
docker logs pi_api_flask-pi_1
```
In this case `pi_api_flask-pi_1` is the container name. This command will show this output in the terminal

```
* Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: on
 * Running on all addresses.
   WARNING: This is a development server. Do not use it in a production deployment.
 * Running on http://172.26.0.2:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 132-961-293
```

The most important from this ouput is the line that says **Running on http://172.26.0.2:5000/ (Press CTRL+C to quit)** because the url (http://172.26.0.2:5000/) is where the API is running. If you click that link, you will be able to see the Swagger Documentation for this API to **Try it out** the endpoint. The image below show the Swagger documentation and the endpoint from the API to get the Response.

![Swagger_1](pi_api_1.png)
![Swagger_2](pi_api_2.png)

# Testing

In this case, to run the tests was used **unittest** and only was testes the utils because is the core of the response for the API. You need to run the following command to run the tests

```bash
python3 -m unittest tests.test_utils
```

# Possible improvements

## Add JWT Authentication

One of the considerations for this exercises was adding some authentication stuff. In this case I try to use an specific library called `Flask-JWT-Extended` but it was not possible to implement it due to lack of time. Other possible library to use is `pyjwt` that can work fine with `Flask Restx` library.

## More test

Maybe adding some integrations test can be improve the API implementation.
