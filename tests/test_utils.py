import unittest
from endpoints.pi.utils import get_n_decimals_pi, get_n_digit

class TestUtilsMethods(unittest.TestCase):

    def test_get_n_decimals_pi_correct(self):
        result = get_n_decimals_pi(5)
        self.assertEqual(result, '3.14159')
    def test_get_n_decimals_pi_with_0(self):
        result = get_n_decimals_pi(0)
        self.assertEqual(result, '3')
    def test_get_n_decimals_pi_gt_12(self):
        # I assume that when a number is greater than 12, it return native np.pi value 
        result = get_n_decimals_pi(13)
        self.assertEqual(result, '3.141593')
    def test_get_n_decimals_pi_lt_0(self):
        # I assume that when a number is greater than 12, it return native np.pi value 
        result = get_n_decimals_pi(-1)
        self.assertEqual(result, '3')
    def test_get_n_digit_correct(self):
        result = get_n_digit(3.14159, 5)
        self.assertEqual(result, '9')

    def test_get_n_digit_incorrect(self):
        result = get_n_digit(3.14, 5)
        self.assertIsInstance(result, int)
        self.assertEqual(result, -1)